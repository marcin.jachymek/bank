package com.wujq.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wujq.configuration.ApplicationContextProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.time.LocalDateTime;

public class BaseTest {

    public static MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    public static String ACCOUNTS_MAPPING = "/api/v1/accounts/";
    public static String EXCHANGE_RATES_MAPPING = "/api/v1/exchange-rates/";
    public static String TX_MAPPING = "/api/v1/transactions/";
    public static String TX_VALUE_IN_EURO = "/transaction_value_in_euro/";

    protected MockMvc mockMvc;
    protected ApplicationContext ctx;
    protected Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
            .create();

    public EntityManager getEntityManager() {
        ctx = ApplicationContextProvider.getContext();
        return ((SessionFactoryImpl) ctx.getBean("sessionFactory")).createEntityManager();
    }
}
