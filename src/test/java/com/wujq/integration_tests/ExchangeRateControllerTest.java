package com.wujq.integration_tests;

import com.google.common.collect.Lists;
import com.wujq.config.BankTestContext;
import com.wujq.controller.v1.ExchangeRateController;
import com.wujq.model.ExchangeRate;
import com.wujq.utils.BaseTest;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BankTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class ExchangeRateControllerTest extends BaseTest {

    @Autowired
    ExchangeRateController exchangeRateController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(exchangeRateController).build();
    }

    @Test
    public void getAllExchangeRatesTest() throws Exception {
        mockMvc.perform(get(EXCHANGE_RATES_MAPPING))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void successfulExchangeRateCreationTest() throws Exception {
        ExchangeRate exchangeRate = getExchangeRate();
        String exchangeRateJson = gson.toJson(exchangeRate);
        mockMvc.perform(post(EXCHANGE_RATES_MAPPING).content(exchangeRateJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void saveOrUpdateManyExchangeRatesTest() throws Exception {
        ExchangeRate er1 = getExchangeRate();
        ExchangeRate er2 = getExchangeRate();
        List<ExchangeRate> erToUpdate = Lists.newArrayList(er1, er2);
        String accountsToUpdateJson = gson.toJson(erToUpdate);
        System.out.println(accountsToUpdateJson);
        mockMvc.perform(put(EXCHANGE_RATES_MAPPING).content(accountsToUpdateJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void createAndUpdateExchangeRatesTest() throws Exception {
        MvcResult result;

        //Create exchange rate
        ExchangeRate exchangeRate = getExchangeRate();
        String exchangeRateToSaveJson = gson.toJson(exchangeRate);
        System.out.println(exchangeRateToSaveJson);
        mockMvc.perform(post(EXCHANGE_RATES_MAPPING).content(exchangeRateToSaveJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();

        //Get exchange rate to update
        result = mockMvc.perform(get(EXCHANGE_RATES_MAPPING + 1))
                .andExpect(status().isOk())
                .andReturn();

        //Check if rate is equal to one
        Assert.assertTrue(result.getResponse().getContentAsString().contains("\"rate\":1"));

        //Update rate to 2
        exchangeRate.setId(1L);
        exchangeRate.setRate(BigDecimal.valueOf(23));
        mockMvc.perform(put(EXCHANGE_RATES_MAPPING + 1).content(gson.toJson(exchangeRate)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isAccepted())
                .andReturn();

        //Get updated exchange rate to update
        result = mockMvc.perform(get(EXCHANGE_RATES_MAPPING + 1))
                .andExpect(status().isOk())
                .andReturn();

        //Check updated rate
        Assert.assertTrue(result.getResponse().getContentAsString().contains("\"rate\":23"));

    }

    private ExchangeRate getExchangeRate() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCreatedAt(LocalDateTime.now());
        exchangeRate.setEndsAt(LocalDateTime.now());
        exchangeRate.setStartsAt(LocalDateTime.now());
        exchangeRate.setUpdatedAt(LocalDateTime.now());
        exchangeRate.setRate(BigDecimal.valueOf(1));
        return exchangeRate;
    }

}