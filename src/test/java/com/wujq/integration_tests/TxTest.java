package com.wujq.integration_tests;

import com.wujq.config.BankTestContext;
import com.wujq.controller.v1.ExchangeRateController;
import com.wujq.controller.v1.TxController;
import com.wujq.model.Account;
import com.wujq.model.ExchangeRate;
import com.wujq.model.Tx;
import com.wujq.utilities.BaseCtrl;
import com.wujq.utils.BaseTest;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BankTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class TxTest extends BaseTest {

    @Autowired
    TxController txController;

    @Autowired
    ExchangeRateController exchangeRateController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(txController).build();
    }

    @Test
    public void successfulTxCreationTest() throws Exception {
        Tx tx = createTransaction();
        String transactionJson = gson.toJson(tx);
        mockMvc.perform(post(TX_MAPPING).content(transactionJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();

    }

    @Test
    public void seeTransactionValueInEuro() throws Exception {
        //Create transaction with value in PLN equal to 4
        Tx tx = createTransaction();
        String transactionJson = gson.toJson(tx);
        mockMvc.perform(post(TX_MAPPING).content(transactionJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();

        //Set exchange rate for transaction to 4 for transaction created before
        ExchangeRate exchangeRate = createExchangeRate();
        String exchangeRateJson = gson.toJson(exchangeRate);
        setupMockForController(exchangeRateController).perform(post(EXCHANGE_RATES_MAPPING).content(exchangeRateJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();

        //Get the value in Euro for transaction
        MvcResult result = mockMvc.perform(get(TX_MAPPING + 1 + TX_VALUE_IN_EURO))
                .andExpect(status().isOk())
                .andReturn();

        //Check if value in Euro for transaction is equal to 1

        Assert.assertEquals("1.00", result.getResponse().getContentAsString());
    }

    private MockMvc setupMockForController(BaseCtrl controller) {
        return MockMvcBuilders.standaloneSetup(controller).build();
    }

    private Tx createTransaction() {
        Tx tx = new Tx();
        tx.setAmount(BigDecimal.valueOf(4));
        tx.setCreatedAt(LocalDateTime.now());
        tx.setDestinationAccount(new Account());
        tx.setSourceAccount(new Account());
        tx.setGroupingId(1);
        return tx;
    }

    private ExchangeRate createExchangeRate() {
        ExchangeRate exchangeRate = new ExchangeRate();
        LocalDateTime now = LocalDateTime.now();
        exchangeRate.setCreatedAt(LocalDateTime.now());
        exchangeRate.setEndsAt(now.plusDays(1));
        exchangeRate.setStartsAt(now.minusDays(1));
        exchangeRate.setUpdatedAt(LocalDateTime.now());
        exchangeRate.setRate(BigDecimal.valueOf(4));
        return exchangeRate;
    }

}
