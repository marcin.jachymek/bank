package com.wujq.integration_tests;

import com.google.common.collect.Lists;
import com.wujq.config.BankTestContext;
import com.wujq.controller.v1.AccountController;
import com.wujq.model.Account;
import com.wujq.utils.BaseTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {BankTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class AccountControllerTest extends BaseTest {

    @Autowired
    AccountController accountController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
    }


    @Test
    public void getAllAccountsTest() throws Exception {
        mockMvc.perform(get(ACCOUNTS_MAPPING))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void successfulAccountCreationTest() throws Exception {
        Account account = new Account();
        String accountJson = gson.toJson(account);
        mockMvc.perform(post(ACCOUNTS_MAPPING).content(accountJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void saveOrUpdateManyAccountsTest() throws Exception {
        Account account1 = new Account();
        Account account2 = new Account();
        List<Account> accountsToUpdate = Lists.newArrayList(account1, account2);
        String accountsToUpdateJson = gson.toJson(accountsToUpdate);
        mockMvc.perform(put(ACCOUNTS_MAPPING).content(accountsToUpdateJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }

}