package com.wujq.config;

import com.wujq.converters.LocalDateTimeConverter;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.format.FormatterRegistry;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.wujq"}, excludeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, value = Configuration.class)})
public class BankTestContext {

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        return ProxyDataSourceBuilder
                .create(actualDataSource())
                .name("Batch-Insert-Logger")
                .logQueryToSysOut()
                .build();
    }

    public DataSource actualDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }

    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateTimeConverter("yyyy-MM-dd'T'HH:mm:ss.SSS"));
    }

    @Bean(name = "sessionFactory")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean session = new LocalSessionFactoryBean();
        session.setPackagesToScan("com.wujq.model");
        session.setDataSource(dataSource());
        session.setHibernateProperties(hibernateProperties());
        return session;
    }

    @Bean(name = "transactionManager")
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory().getObject());

        return txManager;
    }

    private Properties hibernateProperties() {
        return new Properties() {
            {
//                setProperty("hibernate.show_sql", "true");
                setProperty("hibernate.show_sql", "false");
                setProperty("hibernate.hbm2ddl.auto", "create-drop");
                setProperty("hibernate.jdbc.batch_size", "5");
                setProperty("hibernate.order_inserts", "true");
                setProperty("hibernate.order_updates", "true");
                setProperty("hibernate.jdbc.fetch_size", "10");
                setProperty("hibernate.jdbc.batch_versioned_data", "true");
            }
        };
    }
}