INSERT INTO exchange_rate (starts_at, ends_at, rate, created_at, updated_at)
VALUES ('2019-03-01 00:00:00.000000', '2019-03-01 23:59:59.999999', 1.125, '2019-02-17 17:46:48.075409',
        '2019-02-17 17:46:48.075409');

INSERT INTO exchange_rate (starts_at, ends_at, rate, created_at, updated_at)
VALUES ('2019-03-02 00:00:00.000000', '2019-03-02 23:59:59.999999', 1.213, '2019-02-17 17:46:48.075409',
        '2019-02-17 17:46:48.075409');

INSERT INTO exchange_rate (starts_at, ends_at, rate, created_at, updated_at)
VALUES ('2019-03-03 00:00:00.000000', '2019-03-03 23:59:59.999999', 1.383, '2019-02-17 17:46:48.075409',
        '2019-02-17 17:46:48.075409');

DO $$
  DECLARE
    count        BIGINT;
  BEGIN
    count = 1;
    LOOP
      EXIT WHEN count > 100;
      INSERT INTO account(id) VALUES (count);
      count = count + 1;
    END LOOP;
  END ;
  $$;

DO $$
  DECLARE
    count        BIGINT;
  BEGIN
    count = 0;
    LOOP
      EXIT WHEN count > 8761;
      INSERT INTO exchange_rate(starts_at, ends_at, rate)
      VALUES (TIMESTAMP '2019-01-01 00:00:00' + interval '1h' * count , TIMESTAMP '2019-01-01 00:59:59.999999' + interval '1h' * count, (random() * 1 + 1)::decimal(10, 3));
      count = count + 1;
    END LOOP;
  END ;
  $$;

DO $$
  DECLARE
    count        BIGINT;
    dst_id2      BIGINT;
    amount2      DECIMAL(10, 3);
    grouping_id2 BIGINT;
    created_at2  TIMESTAMP WITH TIME ZONE;
  BEGIN
    count = 1;
    LOOP
      EXIT WHEN count > 100000;

      SELECT floor(random() * 50 + 2)::bigint INTO dst_id2;
      SELECT (random() * 1000 + 1)::decimal(10, 3) INTO amount2;
      SELECT floor(random() * 3 + 1)::bigint INTO grouping_id2;
      SELECT TIMESTAMP '2019-01-01 00:00:01' +
             random() * (TIMESTAMP '2019-12-31 23:59:59' -
                         TIMESTAMP '2019-01-01 00:00:01') INTO created_at2;

      INSERT INTO tx(src_id, dst_id, grouping_id, amount, created_at)
      VALUES (1, dst_id2, grouping_id2, amount2, created_at2);

      count = count + 1;
    END LOOP;
  END;
  $$;