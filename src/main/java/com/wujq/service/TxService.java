package com.wujq.service;

import com.wujq.model.ExchangeRate;
import com.wujq.model.ExchangeRate_;
import com.wujq.model.Tx;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TxService extends BaseService<Tx> {

    public BigDecimal getTransactionValue(long transactionId) {
        Tx transaction = this.findOne(transactionId);
        return transaction
                .getAmount()
                .divide(getExchangeRateForTransactionDate(transaction.getCreatedAt()), 2, RoundingMode.HALF_UP);
    }

    private BigDecimal getExchangeRateForTransactionDate(LocalDateTime transactionDate) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ExchangeRate> query = cb.createQuery(ExchangeRate.class);
        Root<ExchangeRate> exchangeRate = query.from(ExchangeRate.class);

        Predicate lessThanOrEqualToDate
                = cb.lessThanOrEqualTo(exchangeRate.get(ExchangeRate_.startsAt), transactionDate);
        Predicate greaterThanOrEqualToDate
                = cb.greaterThanOrEqualTo(exchangeRate.get(ExchangeRate_.endsAt), transactionDate);
        Predicate betweenDates
                = cb.and(greaterThanOrEqualToDate, lessThanOrEqualToDate);

        query.select(exchangeRate).where(betweenDates);
        List<ExchangeRate> exchangeRates = entityManager.createQuery(query).getResultList();
        return exchangeRates.stream().findFirst().get().getRate();
    }

}
