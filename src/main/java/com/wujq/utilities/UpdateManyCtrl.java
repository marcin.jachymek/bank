package com.wujq.utilities;

import com.wujq.model.BaseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UpdateManyCtrl<E extends BaseEntity> extends BaseCtrl<E> {

    @PutMapping(consumes = {"application/json"})
    default ResponseEntity updateMany(@RequestBody List<E> listOfUpdates) {
        getService().updateMany(listOfUpdates);
        return new ResponseEntity(HttpStatus.OK);
    }
}
