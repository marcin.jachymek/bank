package com.wujq.utilities;

import com.wujq.model.BaseEntity;
import com.wujq.service.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface BaseCtrl<E extends BaseEntity> {

    BaseService<E> getService();

    @GetMapping
    default ResponseEntity<List<E>> getAll() {
        return new ResponseEntity(getService().findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    default ResponseEntity<E> getById(@PathVariable("id") long id) {
        return new ResponseEntity(getService().findOne(id), HttpStatus.OK);
    }
}
