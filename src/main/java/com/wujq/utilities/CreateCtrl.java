package com.wujq.utilities;

import com.wujq.model.BaseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface CreateCtrl<E extends BaseEntity> extends BaseCtrl<E> {

    @PostMapping(consumes = {"application/json"})
    default ResponseEntity createNew(@RequestBody E objectToCreate) {
        getService().save(objectToCreate);
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
