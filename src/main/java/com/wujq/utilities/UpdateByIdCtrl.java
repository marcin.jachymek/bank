package com.wujq.utilities;

import com.wujq.model.BaseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface UpdateByIdCtrl<E extends BaseEntity> extends BaseCtrl<E> {

    @PutMapping("/{id}")
    default ResponseEntity updateById(@PathVariable("id") long id, @RequestBody E objectToUpdate) {
        return new ResponseEntity(getService().update(objectToUpdate), HttpStatus.ACCEPTED);
    }
}
