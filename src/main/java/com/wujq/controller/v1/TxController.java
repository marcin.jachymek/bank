package com.wujq.controller.v1;

import com.wujq.model.Tx;
import com.wujq.service.BaseService;
import com.wujq.service.TxService;
import com.wujq.utilities.BaseCtrl;
import com.wujq.utilities.CreateCtrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/transactions/")
public class TxController implements BaseCtrl<Tx>, CreateCtrl<Tx> {
    @Autowired
    TxService txService;

    @Override
    public BaseService getService() {
        return txService;
    }

    @GetMapping("/{id}/transaction_value_in_euro/")
    public ResponseEntity getTxValueInEuro(@PathVariable("id") long id) {
        return new ResponseEntity(txService.getTransactionValue(id), HttpStatus.OK);
    }
}