package com.wujq.controller.v1;

import com.wujq.model.ExchangeRate;
import com.wujq.service.BaseService;
import com.wujq.service.ExchangeRateService;
import com.wujq.utilities.BaseCtrl;
import com.wujq.utilities.CreateCtrl;
import com.wujq.utilities.UpdateByIdCtrl;
import com.wujq.utilities.UpdateManyCtrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/exchange-rates/")
public class ExchangeRateController implements BaseCtrl<ExchangeRate>, UpdateManyCtrl<ExchangeRate>, CreateCtrl<ExchangeRate>, UpdateByIdCtrl<ExchangeRate> {
    @Autowired
    ExchangeRateService exchangeRateService;

    @Override
    public BaseService getService() {
        return exchangeRateService;
    }
}
