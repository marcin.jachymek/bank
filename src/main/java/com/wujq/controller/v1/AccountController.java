package com.wujq.controller.v1;

import com.wujq.model.Account;
import com.wujq.service.AccountService;
import com.wujq.service.BaseService;
import com.wujq.utilities.BaseCtrl;
import com.wujq.utilities.CreateCtrl;
import com.wujq.utilities.UpdateManyCtrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/v1/accounts/")
public class AccountController implements BaseCtrl<Account>, CreateCtrl<Account>, UpdateManyCtrl<Account> {

    @Autowired
    private AccountService accountService;

    @Override
    public BaseService getService() {
        return accountService;
    }
}
