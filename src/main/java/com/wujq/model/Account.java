package com.wujq.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "account")
public class Account extends BaseEntity {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "gen_account_id_seq")
    @SequenceGenerator(name = "gen_account_id_seq", sequenceName = "account_id_seq", allocationSize = 1)
    private long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
