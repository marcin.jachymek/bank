package com.wujq.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.SEQUENCE;
import static org.hibernate.annotations.CascadeType.ALL;

@Entity
@Table(name = "tx")
public class Tx extends BaseEntity {

    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "gen_tx_id_seq")
    @SequenceGenerator(name = "gen_tx_id_seq", sequenceName = "tx_id_seq", allocationSize = 1)
    Long id;

    @Column(name = "grouping_id")
    long groupingId;

    @Column(columnDefinition = "numeric(15,3)")
    BigDecimal amount;

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    LocalDateTime createdAt;

    @ManyToOne
    @Cascade(ALL)
    @JoinColumn(name = "src_id", nullable = false, foreignKey = @ForeignKey(name = "tx_src_id_fkey"))
    Account sourceAccount;

    @ManyToOne
    @Cascade(ALL)
    @JoinColumn(name = "dst_id", nullable = false, foreignKey = @ForeignKey(name = "tx_dst_id_fkey"))
    Account destinationAccount;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGroupingId() {
        return groupingId;
    }

    public void setGroupingId(long groupingId) {
        this.groupingId = groupingId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }
}
